const { FgGreen, Reset, FgRed } = require('./colors')

const validateTranslationFiles = ({ en, ko }) => {
  const enKeys = Object.keys(en)
  const koKeys = Object.keys(ko)

  console.log(Reset, 'Checking translation files outer keys ...')
  const isOuterKeysSame = areArraysSame([enKeys, koKeys])

  if (isOuterKeysSame) {
    console.log(FgGreen, 'Success: ', Reset, 'Translation outer keys are same ✅', Reset)
  } else {
    console.log(FgRed, 'Failed : ', Reset, 'Translation outer keys are not same. ❗', Reset)
    throw new Error('Please check outer keys of translation json files')
  }
  console.log(FgGreen, '✅  all finished with success ✅\n', Reset)
}
module.exports = validateTranslationFiles

const areArraysSame = arrayOfArrays => {
  const maxLength = Math.max(...arrayOfArrays.map(arr => arr.length))

  for (let index = 0; index < maxLength; index++) {
    const [firstArr, ...rest] = arrayOfArrays
    const isDifferent = rest.some(arr => arr[index] !== firstArr[index])
    if (isDifferent) {
      console.log(FgRed, `Value at index ${index} is not same.`, Reset, 'It is', Bright, `"${firstArr[index]}"`, Reset, 'for first element ⁉')
      return false
    }
  }
  return true
}
