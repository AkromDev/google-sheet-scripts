const en = require('../data/en.json')
const ko = require('../data/ko.json')
const { FgCyan, Reset } = require('./colors')
const forceSameStructureForFiles = require('./forceSameStructureForFiles')

const checkCurrTranslationFiles = () => {
  console.log(FgCyan, '\n########### Validating current translation files ##############\n', Reset)
  forceSameStructureForFiles({ en, ko })
  console.log(FgCyan, '########### Finished validating current translations ##############\n', Reset)
}

checkCurrTranslationFiles()
