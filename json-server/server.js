
const express = require('express')
const app = express()
const port = 3000

const en = require('../data/en.json')
const ko = require('../data/ko.json')

app.get('/', (req, res) => res.send(`
  <h2 style="text-align:center; margin-top: 50px">Welcome to translations page</h2>
  <ul>
    <li style="margin-top: 10px"><a href="/en" target="_blank">English</a></li>
    <li style="margin-top: 10px"><a href="/ko" target="_blank">Korean</a></li>
  </ul>
`))

app.get('/en', function (req, res) {
  res.header("Content-Type",'application/json');
  res.send(JSON.stringify({data: en}));

})
app.get('/ko', function (req, res) {
  res.header("Content-Type",'application/json');
  res.send(JSON.stringify({data: ko}));
})
app.get('/in', function (req, res) {
  res.header("Content-Type",'application/json');
  res.send(JSON.stringify({data: simpleIn}));
})

app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`))