
const fs = require('fs')
const path = require('path')

function writeNewJsonFile (FILE_PATH, fileName, file) {
  fs.writeFile(path.join(FILE_PATH, `${fileName}.json`), JSON.stringify(file, null, 4), 'utf8', (err) => {
    if (err) throw err
    console.log(`${fileName}.json done`)
  })
}
function createFolder (FOLDER_PATH) {
  if (!fs.existsSync(FOLDER_PATH)) {
    fs.mkdirSync(FOLDER_PATH)
  }
}
module.exports = {
  writeNewJsonFile,
  createFolder
}
