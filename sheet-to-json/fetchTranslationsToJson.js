const path = require('path')
const { createFolder, writeNewJsonFile } = require('./utils')
const authenticateAndFetch = require('./authenticateAndFetch')

const allFiles = [
// colIndex is the index of column where this locale translations are stored
  { name: 'en', colIndex:1 },
  { name: 'ko', colIndex:2 },
]
var TARGET_FOLDER_PATH = path.join(__dirname,'..', 'output')
const SPREAD_SHEET_ID = ''
const NUMBER_OF_ROWS = 800
const range = `A2:L${NUMBER_OF_ROWS}`

const convertRowsToJson = (err, res) => {
  if (err) return console.log('The API returned an error: ' + err)
  const rows = res.data.values
  if (rows.length) {
    createFolder(TARGET_FOLDER_PATH)
    allFiles.forEach(({ name, colIndex }) => {
      let finalObj = {}
      console.log('rows ',rows)
      rows.map((row) => {
        const key = row[0]
        const value = row[colIndex]
        finalObj[key] = value
      })
      writeNewJsonFile(TARGET_FOLDER_PATH, name, finalObj)
    })
  } else {
    console.log('No data found.')
  }
}

authenticateAndFetch({
  callbackFunc: convertRowsToJson,
  SPREAD_SHEET_ID,
  range
})
