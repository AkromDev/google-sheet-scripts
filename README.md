# Working with Translations

## Uploading local translations to google sheet

First we need to upload local translations to google sheet. This step has to be done for the time only. Once all translations are uploaded to google sheet, you can add new translations or edit existing translations in google sheet. You can either do this step manually copying or do with script. To understand how script works, follow [this tutorial](https://youtu.be/EXKhVQU37WM). I have already created the script for damogo-cust project and you can find it `json-to-sheet` folder in this repo. Simply copy the script and paste it in sheet script editor.

> Note! Before uploading translations to google sheet, make sure that all language translations have same structure so that keys for a particular language does not get mixed.
> <br>
> In order to validate customer app translations, first copy customer app translations in `Lang.ts` and paste them in `data/customer/translations.js` and you need export translation objects as `simpleEn, simpleIn,simpleKor`. Then you can run `npm run validate` script to check if these translation objects have same scructure with all keys in same order.
> <br>
> Same goes for `store app` but you need to paste translation objects to `data/store/translations.js` and validation script for store app is `npm run validate`

## Fetching translations from google sheet

1.  Run `npm run fetch` for customer app & `npm run fetch:store` for store app in the root directory
2.  First time, you need to give access to google sheet and the script creates token.json. The token has expiration date. So if you token error, just delete token.json and re-run the command. To get the token, visit the link in terminal and give permissions shown in images below and paste the token in the terminal and press Enter.

3.  Fetched Translations will be written in `/data/[project]/[lang].json`. Copy translations and paste it in Lang.ts in damogo repo and update SimpleKey type.
